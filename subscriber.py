import ssl
import json
import paho.mqtt.client as mqtt


config_file = open("publisher_config.json", "r")
config_data = json.load(config_file)
config_file.close()


def on_connect(client, userdata, flags, rc):
    # print("Connected with result code "+str(rc))
    client.subscribe(topic="my_message", qos=1)

def on_message(client, userdata, msg):
    print(str(msg.payload.decode("utf-8")))
    

client = mqtt.Client(client_id=config_data['client_id'], clean_session=False)
client.username_pw_set(username="commandandcontrol", password="Qpc423hwdM")
client.tls_set(tls_version=ssl.PROTOCOL_TLSv1_2)
client.connect("cwlicc.zapto.org", 8883, 60)
client.on_connect = on_connect
client.on_message = on_message
client.loop_forever()
