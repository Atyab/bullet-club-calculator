import requests


def send_wire_pusher_message(id, title, message):
    get_request_query_data = {'id': id,
                              'title': title,
                              'message': message,
                              'type': 'monitoring'}

    response = requests.get("https://wirepusher.com/send", params=get_request_query_data)
    print "[START]"
    print "[Request URL] ", response.url
    print "[Response DATA] ", response.text
    print "[END]"

send_wire_pusher_message(id='bF9xmpgfj', title='A message for you', message='Hello Shafay')
send_wire_pusher_message(id='mxkCmpgfC', title='A message for you', message='Hello Astle')
